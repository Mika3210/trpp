from main import func


def test1():
    assert func(150, "СМ", "КМ") == 0.0015


def test2():
    assert func(3, "КМ", "М") == 3000


def test3():
    assert func(8000, "ММ", "СМ") == 800


def test4():
    assert func(15500, "ММ", "М") == 15.5


# test1()
# test2()
# test3()
# test4()