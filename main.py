def func(Number, s1, s2):
    s1 = s1.upper()
    s2 = s2.upper()
    a = 0
    file = open("test.txt", "a")


    if s1 == "ММ":
        if s2 == "ММ":
            a = Number
        elif s2 == "СМ":
            a = Number / 10
        elif s2 == "ДМ":
            a = Number / 100
        elif s2 == "М":
            a = Number / 1000
        elif s2 == "КМ":
            a = Number / 1000000
    elif s1 == "СМ":
        if s2 == "ММ":
            a = Number * 10
        elif s2 == "СМ":
            a = Number
        elif s2 == "ДМ":
            a = Number / 10
        elif s2 == "М":
            a = Number / 100
        elif s2 == "КМ":
            a = Number / 100000
    elif s1 == "ДМ":
        if s2 == "ММ":
            a = Number * 100
        elif s2 == "СМ":
            a = Number * 10
        elif s2 == "ДМ":
            a = Number
        elif s2 == "М":
            a = Number / 10
        elif s2 == "КМ":
            a = Number / 10000
    elif s1 == "М":
        if s2 == "ММ":
            a = Number * 1000
        elif s2 == "СМ":
            a = Number * 100
        elif s2 == "ДМ":
            a = Number * 10
        elif s2 == "М":
            a = Number
        elif s2 == "КМ":
            a = Number / 1000
    elif s1 == "КМ":
        if s2 == "СМ":
            a = Number * 100000
        elif s2 == "ДМ":
            a = Number * 10000
        elif s2 == "М":
            a = Number * 1000
        elif s2 == "ММ":
            a = Number * 1000000
        elif s2 == "КМ":
            a = Number

    file.write(str(Number) + " " + s1 + " = " + str(a) + " " + s2 + "\n")

    if (s1 != "ММ" and s1 != "СМ" and s1 != "ДМ" and s1 != "М" and s1 != "КМ") or \
            (s2 != "ММ" and s2 != "СМ" and s2 != "ДМ" and s2 != "М" and s2 != "КМ"):
        file.write("Ошибка ввода\n")
    return a